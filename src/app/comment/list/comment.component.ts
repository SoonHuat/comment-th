import { Component, OnInit } from '@angular/core';
import { CommentService } from 'src/shared/services/comment.service';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'comment',
  templateUrl: './comment.component.html'
})
export class CommentComponent implements OnInit {
  comments: any;
  posts: any;
  page: number = 1;
  pageSize: number = 10;

  constructor(private commentService: CommentService,
    private router: Router) { }

  async ngOnInit() {
    this.posts = await this.commentService.getPosts();
    this.comments = await this.commentService.getComments();
    _.each(this.posts, post => {
      const postComments = _.filter(this.comments, comment => {
        return comment.postId == post.id;
      });
      post.comments = postComments;
      if (post.comments.length > 5) {
        console.log(`${post.id} ${post.comments.length}`);
      }
    });
    this.posts = _.sortBy(this.posts, ['comments.length']);
    console.log(this.posts);
  }

  onDetailClick(id: number) {
    this.router.navigate([`/comment/${id}`]);
  }
}
