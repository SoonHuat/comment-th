import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable()
export class CommentService {

  private comments: any;
 
  constructor(private http: HttpClient) {}

  async getComments() {
    let promise = new Promise((resolve, reject) => {
      if (this.comments) {
        resolve(this.comments);
      }
      let apiURL = "https://jsonplaceholder.typicode.com/comments";
      this.http.get(apiURL)
        .toPromise()
        .then((res: any) => {
          this.comments = res;
          resolve(res);
        });
    });
    return await promise;
  }

  async getPosts() {
    let promise = new Promise((resolve, reject) => {
      let apiURL = "https://jsonplaceholder.typicode.com/posts";
      this.http.get(apiURL)
        .toPromise()
        .then((res: any) => {
          resolve(res);
        });
    });
    return await promise;
  }

  async getPost(id: string) {
    let promise = new Promise((resolve, reject) => {
      let apiURL = `https://jsonplaceholder.typicode.com/posts/${id}`;
      this.http.get(apiURL)
        .toPromise()
        .then((res: any) => {
          resolve(res);
        });
    });
    return await promise;
  }

  async getUser(id: string) {
    let promise = new Promise((resolve, reject) => {
      let apiURL = `https://jsonplaceholder.typicode.com/users/${id}`;
      this.http.get(apiURL)
        .toPromise()
        .then((res: any) => {
          resolve(res);
        });
    });
    return await promise;
  }
}